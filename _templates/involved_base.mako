<%inherit file="/base.mako"/>
<%!
section='involved'
%>
<%block name="head_title">
Get Involved - SQLAlchemy
</%block>

<h1>Community Guide</h1>

<p>A guide to getting involved with SQLAlchemy.</p>
<ul class="nav">
    <li><a href="/support.html">Get Support</a> - help with using SQLAlchemy</li>
    <li><a href="/participate.html">Participate</a> - reporting bugs and helping others</li>
    <li><a href="/develop.html">Develop</a> - contributing code</li>
    <li><a href="/further.html">Further Reading</a> - great links on how to ask for help</li>
</ul>

${next.body()}